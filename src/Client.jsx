import { Component } from "preact"
import { Provider } from "preact-redux"

import Routes from './routes/'
import store from "./store"

import './style'

export default class Client extends Component {
  render() {
    return (<Provider store={store}>
      <Routes />
    </Provider>)
  }
}
