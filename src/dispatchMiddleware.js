const dispatchMiddleware = store => next => action => {
  let queue = [];

  function dispatch(action) {
    queue = queue.concat([action])
  }
  const actionWithQueue = Object.assign({}, action, {
    dispatch
  });
  const data = next(actionWithQueue)
  queue.forEach(a => store.dispatch(a)); // flush queue
  return data
}

export default dispatchMiddleware;
