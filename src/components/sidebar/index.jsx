import { Component } from "preact"

import ChannelList from "./ChannelList"

import style from "./index.css"

export default class SideBar extends Component {
    render({ channels }, { }) {
        return (<div class={style.sidebar}>
            <div class={style.header_container}><header>
                <a class={style.username}>Username</a>
            </header></div>
            <ChannelList channels={channels} />
        </div>)
    }
}