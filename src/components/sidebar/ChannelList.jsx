import { Component } from "preact"

import style from "./ChannelList.css"

export default class ChannelList extends Component {
    render({ channels }, { }) {
        let channel_html = channels.map((v, i) => { return (<li><a class={style.tag}>#&nbsp;</a>{v}</li>) })
        return (<ul class={style.channel_list}>
            {channel_html}
        </ul>)
    }

}