import {Component} from "preact"

import style from "./LoadingSpiner.css"

export default class LoadingSpiner extends Component {
  render() {
    return (<div class={style.spinner}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>)
  }
}
