import { Component } from "preact"
import LoadingSpiner from "../components/LoadingSpiner"

import { connect } from "preact-redux"
import style from "./Page.css"

@connect((store) => {
  return { loading: store.info.loading }
})
export default class Page extends Component {

  render({
    children,
    loading
  }, { }) {
    return (<div>
      <div class={style.page}>
        {
          (loading || children.length == 0)
            ? (<LoadingSpiner />)
            : children
        }
      </div>
    </div>)
  }
}
