import { Component } from "preact"
import { connect } from "preact-redux"

import { connect as wsConnect } from "../actions/ws"

import style from "./ChatPage.css"

import SideBar from "../components/sidebar"

@connect((store) => {
  return {}
})
export default class ChatPage extends Component {
  connect(e) {
    this.props.dispatch(wsConnect())
  }

  render({ }, { }) {
    return (<div class={style.page}>
      <SideBar channels={["General", "Memes", "Dank Memes", "F"]} />

      <div class={style.mid}>
        <div class={style.header_container}><header>
          <a class={style.tag}>#&nbsp;</a>General
        </header></div>
        <section>
          test <button onClick={this.connect.bind(this)}>connect</button>
        </section>
      </div>
    </div>)
  }
}
