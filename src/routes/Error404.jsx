import { Component } from "preact"
import Page from "./Page"

import style from "./Error404.css"

export default class Error404 extends Component {
  render({ }, { }) {
    return (<Page>
      <h1 class={style.errorCode}>404</h1>
      <p class={style.errorText}>The page you requested was not found.</p>
    </Page>)
  }
}
