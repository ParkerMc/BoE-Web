import {
  combineReducers
} from "redux"

import info from "./infoReducer"
import ws from "./wsReducer"

export default combineReducers({
  info: info,
  ws: ws
})
