export default function reducer(state = {
  loading: false
}, action) {
  switch (action.type) {
    case "LOADING":
      return {
        ...state,
        loading: action.payload
      }
  }
  return state
}
