export default function reducer(state = {
  conn: undefined
}, action) {
  switch (action.type) {
    case "WEBSOCKET_PENDING":
      console.log(action)
      return {
        ...state,
        conn: action.payload
      }
  }
  return state
}
