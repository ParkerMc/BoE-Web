import promise from "redux-promise-middleware"
import {
  applyMiddleware,
  createStore
} from "redux"

import ReduxThunk from 'redux-thunk'
import dispatchMiddleware from "./dispatchMiddleware"

import reducer from "./reducers"

const middleware = applyMiddleware(ReduxThunk, promise, dispatchMiddleware)

export default createStore(reducer, middleware)
