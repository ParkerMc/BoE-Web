import CopyWebpackPlugin from "copy-webpack-plugin"

module.exports = (config) => {
  config.plugins.push(new CopyWebpackPlugin([{
    from: `${__dirname}/src/static`,
    to: `${__dirname}/build/static`
  }]));

  if (config.devServer !== undefined) {
    config.devServer.proxy = [{
      path: "/api/**",
      target: "http://localhost:8888/",
    }];
  }

};
